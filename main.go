package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"path/filepath"
	"sync"

	"github.com/oklog/ulid/v2"
)

var (
	listenAddress     string
	dumpBodyToFileDir string
	mux               sync.Mutex
	healthy           bool
	ready             bool
)

func main() {
	flag.StringVar(&listenAddress, "listen", ":8080", "listen address (default :8080)")
	flag.StringVar(&dumpBodyToFileDir, "dir", "", "dump request body to file, new file for each request in the given directory (default disabled)")
	flag.Parse()

	// Check if the dump directory exists and is writable.
	if dumpBodyToFileDir != "" {
		test := filepath.Join(dumpBodyToFileDir, "test")
		if _, err := os.Create(test); err != nil {
			log.Fatal(fmt.Errorf("failed to create file in dump directory: %w", err))
		}
		if err := os.Remove(test); err != nil {
			log.Fatal(fmt.Errorf("failed to remove file in dump directory: %w", err))
		}
	}

	healthy = true
	ready = true
	http.HandleFunc("/healthz", handleHealthz)
	http.HandleFunc("/readyz", handleReadyz)

	http.HandleFunc("/", handle)
	log.Printf("listening on %s", listenAddress)
	if err := http.ListenAndServe(listenAddress, nil); err != nil {
		log.Println(err)
	}
}

func handle(w http.ResponseWriter, r *http.Request) {
	log.Println("dumping request:")
	defer log.Println("done")
	// Replace r.Body with a tee reader.
	if dumpBodyToFileDir != "" {
		// If flag is set, also dump request body to file.
		fileName := ulid.MustNew(ulid.Now(), nil).String()
		file, err := os.Create(filepath.Join(dumpBodyToFileDir, fileName))
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer file.Close()
		defer func() {
			// Sync the file to disk.
			if err := file.Sync(); err != nil {
				log.Println(err)
				return
			}
			info, err := file.Stat()
			if err != nil {
				log.Println(err)
				return
			}
			log.Printf("dumped request body to file %s (%d bytes)", fileName, info.Size())
		}()
		body := io.TeeReader(r.Body, file)
		r.Body = io.NopCloser(body)
	}
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		log.Println(err)
	}
	if _, err := io.Copy(os.Stderr, bytes.NewBuffer(dump)); err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("X-Backend", hostname)
	if _, err := fmt.Fprintf(w, "<h1>Hello from %s</h1>", hostname); err != nil {
		log.Println(err)
	}
}

func handleHealthz(w http.ResponseWriter, r *http.Request) {
	mux.Lock()
	defer mux.Unlock()

	// Allow for healthcheck to fail on purpose.
	if r.Method == http.MethodDelete {
		healthy = false
		return // Next healthcheck will fail.
	}

	if r.Method == http.MethodPost || r.Method == http.MethodPut {
		healthy = true
		return // Next healthcheck will succeed.
	}

	if healthy {
		return
	}
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func handleReadyz(w http.ResponseWriter, r *http.Request) {
	mux.Lock()
	defer mux.Unlock()

	// Allow for readiness check to fail on purpose.
	if r.Method == http.MethodDelete {
		ready = false
		return // Next readiness check will fail.
	}

	if r.Method == http.MethodPost || r.Method == http.MethodPut {
		ready = true
		return // Next readiness check will succeed.
	}

	if ready {
		return
	}
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}
