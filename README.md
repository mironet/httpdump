# Quick HTTP troubleshooter

This program listens on a given address and dumps HTTP requests verbatim with all headers etc. to stderr.

```bash
./httpdump -listen :8080
2021/02/10 01:00:38 listening on :8080
2021/02/10 01:00:42 dumping request:
GET / HTTP/1.1
Host: localhost:8080
Accept: */*
User-Agent: curl/7.58.0

2021/02/10 01:00:42 done
```

## Run

### Docker

```bash
docker run -p 9090:8080 registry.gitlab.com/mironet/httpdump
```

### Healthcheck and Readiness Check

`/healthz` and `/readyz` can be used here. The results of the check can be deactivated on purpose to test behaviour e.g. in a k8s cluster:

```bash
# Fail health check from now on.
curl -v -X DELETE localhost:8080/healthz

# Fail readiness check from now on.
curl -v -X DELETE localhost:8080/readyz

# Restore health check.
curl -v -X PUT localhost:8080/healthz

# Restore readiness check.
curl -v -X PUT localhost:8080/readyz
```
